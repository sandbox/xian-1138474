<?php
// $Id: livestream.inc,v 1.1.2.16 2009/02/05 03:28:49 aaron Exp $

/**
 * @file
 *   This include processes justin.tv media files for use by emfield.module.
 */

define('MEDIA_JUSTINTV_MAIN_URL', 'http://www.justin.tv/');
define('MEDIA_JUSTINTV_API_INFO', 'http://www.justin.tv/p/api');
define('MEDIA_JUSTINTV_API_APPLICATION_URL', 'http://www.justin.tv/p/api');
define('MEDIA_JUSTINTV_REST_ENDPOINT', 'http://api.justin.tv/api/');

/* http://channel.api.justin.tv/1.0/embed?channel=proshowcase
 * returns the actual video HTML
 *
 * http://channel.api.justin.tv/1.0/livestatus?channel=proshowcase
 * returns a result indicating whether the channel is live or not
 */


/**
 * hook emvideo_PROVIDER_info
 * this returns information relevant to a specific 3rd party video provider
 * @return
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the provider
 *   'url' => the url to the main page for the provider
 *   'settings_description' => a description of the provider that will be posted in the admin settings form
 *   'supported_features' => an array of rows describing the state of certain supported features by the provider.
 *      These will be rendered in a table, with the columns being 'Feature', 'Supported', 'Notes'.
 */
function emvideo_justintv_info() {
  $features = array(
    array(t('Autoplay'), t('Yes'), ''),
    array(t('RSS Attachment'), t('No'), ''),
    array(t('Show related videos'), t('No'), ''),
    array(t('Thumbnails'), t('Yes'), t('')),
    array(t('Custom player colors'), t('No'), ''),
    array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
  );
  return array(
    'module' => 'media_justintv',
    'provides' => array('emvideo'),
    'provider' => 'justintv',
    'name' => t('Justin.TV'),
    'url' => MEDIA_JUSTINTV_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from <a href="@livestream" target="_blank">Livestream</a>. You can learn more about its <a href="@api" target="_blank">API</a> here.', array('@livestream' => MEDIA_JUSTINTV_MAIN_URL, '@api' => MEDIA_JUSTINTV_API_INFO)),
    'supported_features' => $features,
  );
}

/**
 * hook emvideo_PROVIDER_settings
 * this should return a subform to be added to the emvideo_settings() admin settings page.
 * note that a form field will already be provided, at $form['PROVIDER'] (such as $form['livestream'])
 * so if you want specific provider settings within that field, you can add the elements to that form field.
 */
function emvideo_justintv_settings() {
  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('Livestream API'),
    '#description' => t('You will first need to apply for an API Developer Key from the <a href="@livestream" target="_blank">Livestream Developer Profile page</a>. Note that you do not need this key to display Livestream videos or their thumbnails.', array('@livestream' => MEDIA_JUSTINTV_API_APPLICATION_URL)),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['api']['media_justintv_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Livestream API Key'),
    '#default_value' => variable_get('media_justintv_api_key', ''),
    '#description' => t('Please enter your Livestream Developer Key here.'),
  );

  $form['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['player_options']['media_justintv_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('media_justintv_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );
  $form['player_options']['media_justintv_autoplay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto play'),
    '#default_value' => variable_get('media_justintv_autoplay', 1),
    '#description' => t('Automatically play the video when users visit its page.'),
  );
  return $form;
}

/**
 * hook emfield_PROVIDER_data
 *
 * provides an array to be serialised and made available with $item elsewhere
 */
function emvideo_justintv_data($field, $item) {
	
  $data = array();
  
  // create some 'field' version control
  $data['emvideo_justintv_version'] = 1;
  
  $channelname = explode('/', $item['value']);
	
	$feedrequesturl = 'http://api.justin.tv/api/clip/show/' . $channelname[2] . '.xml';
	
	$response = drupal_http_request($feedrequesturl);
	
	if ($response->code == 200) {
	
		$xml = simplexml_load_string($response->data);
		
		$data['duration'] = (string) $xml->clip->length;
		$data['thumbnail']['url'] = (string) $xml->clip->image_url_large;
		
	}
	
  return $data;
  
}

/**
 *
 */
function emvideo_justintv_rss($item, $teaser = NULL) {
  if ($item['value']) {
    if (!empty($item['data']['emvideo_livestream_data_version']) && $item['data']['emvideo_livestream_data_version'] >= 1) {
      $data = $item['data'];
    }
    else {
      $data = emvideo_livestream_data(NULL, $item);
    }

    $file = array();
    if (is_array($data['flash'])) {
      $file['filepath'] = $data['flash']['url'];
      $file['filesize'] = $data['flash']['size'];
      $file['filemime'] = $data['flash']['mime'];
    }
    $file['thumbnail']['filepath'] = $data['thumbnail']['url'];

    return $file;
	}
}

/**
 * hook emvideo_PROVIDER_extract
 * this is called to extract the video code from a pasted URL or embed code.
 * @param $embed
 *   an optional string with the pasted URL or embed code
 * @return
 *   either an array of regex expressions to be tested, or a string with the video code to be used
 *   if the hook tests the code itself, it should return either the string of the video code (if matched), or an empty array.
 *   otherwise, the calling function will handle testing the embed code against each regex string in the returned array.
 */
function emvideo_justintv_extract($embed = '') {
  return array(
   // '@.justin.tv\/([\w]+)\/b\/([\d]+)@i',
		'@justin\.tv/([^"\&]+)@i',
  );
}


/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site
 *  @param $video_code
 *    the string containing the video to watch
 *  @return
 *    a string containing the URL to view the video at the original provider's site
 */
function emvideo_justintv_embedded_link($video_code) {
  return 'http://www.justin.tv/'. $video_code;
}

/**
 * The embedded flash displaying the livestream video.
 */
function theme_emvideo_justintv_flash($embed, $width, $height, $autoplay=TRUE, $options = array()) {

	static $count;
	
	$output = '';
	
	if ($embed) {	
  
		$video_details = explode('/', $embed);
  	
		$fullscreen = isset($options['fullscreen']) ? $options['fullscreen'] : variable_get('media_justintv_full_screen', 1);
		$fullscreen_value = $fullscreen ? "true" : "false";

		// This is a little funky. Essentially it ignores whatever is passed in to $autoplay and retrieves the setting or
		// takes a value from the options array. Also, don't I need to convert the & into &amp;?
		$autoplay = isset($options['autoplay']) ? $options['autoplay'] : variable_get('media_justintv_autoplay', 1);
		$autoplay_value = $autoplay ? 'true' : 'false';

		$div_id = isset($options['div_id']) ? $options['div_id'] : 'media-justintv-flash-wrapper-'. $count;

		$output .= '
		<div id="' . $div_id . '">
			<object type="application/x-shockwave-flash" height="' . $height . '" width="' . $width . '" id="clip_embed_player_flash" data="http://www.justin.tv/widgets/archive_embed_player.swf" bgcolor="#000000">
				<param name="movie" value="http://www.justin.tv/widgets/archive_embed_player.swf" />
				<param name="allowScriptAccess" value="always" />
				<param name="allowNetworking" value="all" />
				<param name="allowFullScreen" value="' . $fullscreen_value . '" />
				<param name="flashvars" value="auto_play=' . $autoplay_value . '&start_volume=25&title=&channel=' . $video_details[0] . '&archive_id=' . $video_details[2] . '" />
			</object> 
		</div>';
	}
	
	return $output;
	
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * returns the external url for a thumbnail of a specific video
 * TODO: make the args: ($embed, $field, $item), with $field/$item provided if we need it, but otherwise simplifying things
 *  @param $field
 *    the field of the requesting node
 *  @param $item
 *    the actual content of the field from the requesting node
 *  @return
 *    a URL pointing to the thumbnail
 */
function emvideo_justintv_thumbnail($field, $item, $formatter, $node, $width, $height) {
	
   if (isset($item['data']['thumbnail']['url'])) {
    return $item['data']['thumbnail']['url'];
  }
  
  $video_details = explode('/', $item['value']);
  
  // Always return the larger image, since we're storing images locally.
  
  $tn = 'http://static-cdn.jtvnw.net/jtv.thumbs/archive-' . $video_details[2] . '-320x240.jpg';

  return $tn;
  
}

/**
 * hook emvideo_PROVIDER_duration($item)
 * Returns the duration of the video in seconds.
 *  @param $item
 *    The video item itself, which needs the $data array.
 *  @return
 *    The duration of the video in seconds.
 */
function emvideo_justintv_duration($item) {
  if (!isset($item['data']['media_justintv_version'])) {
    $item['data'] = emvideo_justintv_data(NULL, $item);
  }
  return isset($item['data']['duration']) ? $item['data']['duration'] : 0;
}

/**
 * hook emvideo_PROVIDER_video
 * this actually displays the full/normal-sized video we want, usually on the default page view
 *  @param $embed
 *    the video code for the video to embed
 *  @param $width
 *    the width to display the video
 *  @param $height
 *    the height to display the video
 *  @param $field
 *    the field info from the requesting node
 *  @param $item
 *    the actual content from the field
 *  @return
 *    the html of the embedded video
 */
function emvideo_justintv_video($embed, $width, $height, $field, $item, &$node, $autoplay) {
  $output = theme('emvideo_justintv_flash', $embed, $width, $height, $autoplay);
  return $output;
}

/**
 * hook emvideo_PROVIDER_preview
 * this actually displays the preview-sized video we want, commonly for the teaser
 *  @param $embed
 *    the video code for the video to embed
 *  @param $width
 *    the width to display the video
 *  @param $height
 *    the height to display the video
 *  @param $field
 *    the field info from the requesting node
 *  @param $item
 *    the actual content from the field
 *  @return
 *    the html of the embedded video
 */
function emvideo_justintv_preview($embed, $width, $height, $field, $item, &$node, $autoplay) {
  $output = theme('emvideo_justintv_flash', $embed, $width, $height, $autoplay);
  return $output;
}

/**
 * Implementation of hook_emfield_subtheme.
 */
function emvideo_justintv_emfield_subtheme() {
    return array(
        'emvideo_justintv_flash'  => array(
            'arguments' => array('embed' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
            'file' => 'providers/justintv.inc'
        )
    );
}